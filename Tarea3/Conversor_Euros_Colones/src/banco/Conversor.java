package banco;

public class Conversor {
    
    private double valor;
    private double valor_euro = 742.13;
    
    public Conversor(){
        
    }
    
    public double Convertir_Euros_a_Colones(){
        
        double resultado=this.getValor()*this.getValor_euro();
        
        return resultado;
        
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getValor_euro() {
        return valor_euro;
    }

    public void setValor_euro(double valor_euro) {
        this.valor_euro = valor_euro;
    }
    
    
            
    
}
