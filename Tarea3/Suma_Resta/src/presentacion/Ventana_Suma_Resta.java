package presentacion;

import javax.swing.JOptionPane;
import negocio.Resultado;

public class Ventana_Suma_Resta extends javax.swing.JFrame {

    Resultado r = new Resultado();

    public Ventana_Suma_Resta() {
        initComponents();
        setTitle("Suma y Resta");
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        caja_texto_1 = new javax.swing.JTextField();
        caja_texto_2 = new javax.swing.JTextField();
        boton_sumar = new javax.swing.JButton();
        boton_restar = new javax.swing.JButton();
        label_1 = new javax.swing.JLabel();
        label_resultado = new javax.swing.JLabel();
        label_2 = new javax.swing.JLabel();
        label_3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 224, 0));
        jPanel1.setForeground(new java.awt.Color(8, 1, 1));

        boton_sumar.setText("Sumar");
        boton_sumar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_sumarActionPerformed(evt);
            }
        });

        boton_restar.setText("Restar");
        boton_restar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_restarActionPerformed(evt);
            }
        });

        label_1.setBackground(new java.awt.Color(14, 12, 12));
        label_1.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        label_1.setForeground(new java.awt.Color(4, 2, 2));
        label_1.setText("El resultado de la operación es:");

        label_resultado.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        label_resultado.setForeground(new java.awt.Color(1, 1, 1));
        label_resultado.setText("0");

        label_2.setBackground(new java.awt.Color(14, 12, 12));
        label_2.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        label_2.setForeground(new java.awt.Color(4, 2, 2));
        label_2.setText("Primer Valor");

        label_3.setBackground(new java.awt.Color(14, 12, 12));
        label_3.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        label_3.setForeground(new java.awt.Color(4, 2, 2));
        label_3.setText("Segundo Valor");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(label_2)
                        .addGap(40, 40, 40)
                        .addComponent(label_3)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(boton_sumar)
                                    .addComponent(caja_texto_1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(caja_texto_2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(boton_restar)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(label_1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(label_resultado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(52, 52, 52))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_2)
                    .addComponent(label_3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(caja_texto_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(caja_texto_1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(boton_sumar)
                    .addComponent(boton_restar))
                .addGap(33, 33, 33)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_1)
                    .addComponent(label_resultado, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void boton_sumarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_sumarActionPerformed

        if (this.caja_texto_1.getText().equals("") && this.caja_texto_2.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Error, no puede dejar espacios en blanco",
                    "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            float valor_1 = Float.parseFloat(this.caja_texto_1.getText());
            float valor_2 = Float.parseFloat(this.caja_texto_2.getText());
            float resultado = r.resultado_suma(valor_1, valor_2);

            this.label_resultado.setText(String.valueOf(resultado));

            this.caja_texto_1.setText("");
            this.caja_texto_2.setText("");
        }


    }//GEN-LAST:event_boton_sumarActionPerformed

    private void boton_restarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_restarActionPerformed

        if (this.caja_texto_1.getText().equals("") && this.caja_texto_2.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Error, no puede dejar espacios en blanco",
                    "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            
            float valor_1 = Float.parseFloat(this.caja_texto_1.getText());
            float valor_2 = Float.parseFloat(this.caja_texto_2.getText());
            float resultado = r.resultado_resta(valor_1, valor_2);

            this.label_resultado.setText(String.valueOf(resultado));

            this.caja_texto_1.setText("");
            this.caja_texto_2.setText("");
        }


    }//GEN-LAST:event_boton_restarActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana_Suma_Resta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana_Suma_Resta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana_Suma_Resta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana_Suma_Resta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana_Suma_Resta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boton_restar;
    private javax.swing.JButton boton_sumar;
    private javax.swing.JTextField caja_texto_1;
    private javax.swing.JTextField caja_texto_2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel label_1;
    private javax.swing.JLabel label_2;
    private javax.swing.JLabel label_3;
    private javax.swing.JLabel label_resultado;
    // End of variables declaration//GEN-END:variables
}
