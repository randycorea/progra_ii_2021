package composicion_1;

public class Contacto {
    
    private String nombre;
    private String telefono;
    
    //Metodo Constructor
    public Contacto(){
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    
}
