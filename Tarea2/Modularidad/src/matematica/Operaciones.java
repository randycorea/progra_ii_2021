package matematica;

public class Operaciones {

    //Variables a Utilizar
    private int n;

    //Metodo Constructor
    public Operaciones() {

    }

    //Metodo para averiguar si el numero es primo
    public boolean Numero_Primo() {

        int i = 0;
        int c = 0;

        for (i = 1; i <= this.getN(); i++) {
            if ((this.getN() % i) == 0) {
                c++;
            }
        }

        if (c == 2) {
            return true;
        } else {
            return false;
        }

    }

    //Metodo para averiguar si el numero es perfecto
    public boolean Numero_Perfecto() {

        int i = 0;
        int c = 0;

        for (i = 1; i < this.getN(); i++) {
            if (this.getN() % i == 0) {
                c += i;
            }
        }

        if (c == this.getN()) {
            return true;
        } else {
            return false;
        }

    }

    //Metodo para averiguar si el numero es un numero capicúa
    public boolean Numero_Capicua() {
        
        int aux=this.getN();
        int in=0;
        int ci=0;
        
        while(aux>0){
            ci=aux%10;
            in=in*10+ci;
            aux=aux/10;
        }
        
        if(this.getN()==in){
            return true;
        }else{
            return false;
        }
        
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

}
