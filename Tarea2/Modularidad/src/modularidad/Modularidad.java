package modularidad;

import java.util.Scanner;
import matematica.Operaciones;

public class Modularidad {

    public static void Usuario() {

        Scanner n = new Scanner(System.in);
        Operaciones op = new Operaciones();

        boolean continuar = true;
        int opcion = 0;
        int numero = 0;

        while (continuar == true) {

            System.out.println("************************");
            System.out.println("**** Menú Principal ****");
            System.out.println("    1. Número Primo     ");
            System.out.println("    2. Número Perfecto  ");
            System.out.println("    3. Número Capicúa   ");
            System.out.println("    4. Salir            ");
            System.out.println("************************");

            System.out.println("");

            System.out.println("************************************");
            System.out.println("Dijite la opcion que desea realizar:");
            System.out.println("************************************");
            opcion = n.nextInt();

            if (opcion == 1) {

                System.out.println("**************************************************");
                System.out.println("Ingrese el numero que desea averiguar si es primo:");
                System.out.println("**************************************************");
                numero = n.nextInt();
                op.setN(numero);

                System.out.println("***************");
                System.out.println(op.Numero_Primo());
                System.out.println("***************");

                System.out.println("");

            }

            if (opcion == 2) {

                System.out.println("*****************************************************");
                System.out.println("Ingrese el numero que desea averiguar si es perfecto:");
                System.out.println("*****************************************************");
                numero = n.nextInt();
                op.setN(numero);

                System.out.println("******************");
                System.out.println(op.Numero_Perfecto());
                System.out.println("******************");

                System.out.println("");

            }

            if (opcion == 3) {

                System.out.println("****************************************************");
                System.out.println("Ingrese el numero que desea averiguar si es capicúa:");
                System.out.println("****************************************************");
                numero = n.nextInt();
                op.setN(numero);

                System.out.println("*****************");
                System.out.println(op.Numero_Capicua());
                System.out.println("*****************");

                System.out.println("");

            }

            if (opcion == 4) {
                System.exit(0);
            }
        }

    }

    public static void main(String[] args) {
        Usuario();
    }

}
