package federacion;

public class Entrenador extends Seleccion_Futbol{
    
    int id_entrenador;
    int id_federacion;
    
    public Entrenador(){
        
    }
    
    public void Planificar_Entrenamiento(){
        
    }

    @Override
    public void Viajar() {
    }

    @Override
    public void Concentrarse() {
    }

    @Override
    public void Entranamientos() {
    }

    @Override
    public void Partido() {
    }

    public int getId_entrenador() {
        return id_entrenador;
    }

    public void setId_entrenador(int id_entrenador) {
        this.id_entrenador = id_entrenador;
    }

    public int getId_federacion() {
        return id_federacion;
    }

    public void setId_federacion(int id_federacion) {
        this.id_federacion = id_federacion;
    }
    
    
    
}
