package federacion;

public abstract class Seleccion_Futbol {

    int id;
    String nombre;
    int fundacion;

    public Seleccion_Futbol() {

    }

    public abstract void Viajar();

    public abstract void Concentrarse();

    public abstract void Entranamientos();

    public abstract void Partido();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getFundacion() {
        return fundacion;
    }

    public void setFundacion(int fundacion) {
        this.fundacion = fundacion;
    }

}
