package modularidad_ejercicio3;

import java.util.Scanner;
import seguridad.Validacion;

public class Modularidad_Ejercicio3 {

    public static void Usuario() {

        Scanner t = new Scanner(System.in);
        
        System.out.println("--------------------");
        System.out.println("Ingrese el usuario: ");
        System.out.println("--------------------");
        String usu = t.nextLine();

        System.out.println("-----------------------");
        System.out.println("Ingrese la contraseña: ");
        System.out.println("-----------------------");
        String con = t.nextLine();
        
        Validacion v = new Validacion(usu, con);
        
        System.out.println(v.Validar_Acceso());
        

    }

    public static void main(String[] args) {
        Usuario();
    }

}
