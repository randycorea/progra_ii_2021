package banco;

public abstract class Cuenta_Abstracta {
    
    String titular;
    double cantidad;
    
    public Cuenta_Abstracta(){
        
    }
    
    public abstract String Mostrar();
    
    public abstract double Ingresar_Dinero(double m);
    
    public abstract double Retirar_Dinero(double m);

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }
    
    
    
}
