package abstracta_ejercicio2;

import banco.*;

public class Abstracta_Ejercicio2 {

    public static void Usuario() {

        Cuenta c = new Cuenta("Randy Corea", 2500);
        System.out.println(c.Mostrar());
        c.Ingresar_Dinero(3000);
        System.out.println(c.Mostrar());
        c.Retirar_Dinero(2000);
        System.out.println(c.Mostrar());
        
        
        Cuenta_Joven cj = new Cuenta_Joven("Randy Corea", 21, 3000); 
        System.out.println(cj.Mostrar());
        c.Ingresar_Dinero(3000);
        System.out.println(cj.Mostrar());
        c.Retirar_Dinero(2000);
        System.out.println(cj.Mostrar());

    }

    public static void main(String[] args) {
        Usuario();
    }

}
