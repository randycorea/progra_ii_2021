package transporte;

public class Coche extends Vehiculo {

    float cilindrada;
    float velocidad;

    public Coche() {
       
    }

    public Coche(String color, int ruedas, float cilindrada, float velocidad) {
        super();
        super.setColor(color);
        super.setRuedas(ruedas);
        this.setCilindrada(cilindrada);
        this.setVelocidad(velocidad);
    }

    public String Mostrar() {

        return super.Mostrar()+String.format("El cilindraje es %f y la velocidad es de %f",
                this.getCilindrada(), this.getVelocidad());

    }

    public float getCilindrada() {
        return cilindrada;
    }

    public void setCilindrada(float cilindrada) {
        this.cilindrada = cilindrada;
    }

    public float getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(float velocidad) {
        this.velocidad = velocidad;
    }

}
