package herencia_ejercicio1;

import transporte.*;

public class Herencia_Ejercicio1 {

    public static void Usuario() {

        Coche c = new Coche();
        c.setCilindrada(2000);
        c.setVelocidad(300);
        c.setColor("Rojo");
        c.setRuedas(4);
        System.out.println(c.Mostrar());;

        Bicicleta b = new Bicicleta();
        b.setColor("Negra");
        b.setRuedas(2);
        b.setTipo("Montañera");
        System.out.println(b.Mostrar());

    }

    public static void main(String[] args) {
        Usuario();
    }

}
