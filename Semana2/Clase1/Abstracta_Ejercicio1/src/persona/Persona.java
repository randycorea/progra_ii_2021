package persona;

public abstract class Persona {
    
    String nombre;
    int edad;
    String cedula;
    
    public Persona(){
        
    }
    
    public abstract boolean Mayor_de_Edad();
    
    public abstract String Mostrar();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    
    
}
