package matematicas;

public class Exponente {
    
    float ex=0;
    float ba=0;
    
    public Exponente (float ex, float ba){
        this.setEx(ex);
        this.setBa(ba);
    }
    
    public double Calculo(){
        return Math.pow(this.getBa(), this.getEx());
    }

    public float getEx() {
        return ex;
    }

    public void setEx(float ex) {
        this.ex = ex;
    }

    public float getBa() {
        return ba;
    }

    public void setBa(float ba) {
        this.ba = ba;
    }
    
    
    
}
