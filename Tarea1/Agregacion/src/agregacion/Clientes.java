package agregacion;

public class Clientes {

    //Definicion de Variables a Utilizar
    private String tipo;
    private String nombre;
    private int cedula;

    //Metodo Constructor
    public Clientes() {

    }

    //Metodos para accesos directo a los atributos
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

}
