package agregacion;

import java.util.Random;
import java.util.Scanner;

public class Agregacion {

    public static void Usuario() {

        Scanner n = new Scanner(System.in);
        Scanner t = new Scanner(System.in);
        
        Random r = new Random();

        Empresa em = new Empresa();
        Clientes cli = new Clientes();

        System.out.println("--------------------------------------");
        System.out.println("Digite el numero de clientes a agregar");
        System.out.println("--------------------------------------");
        int nu = n.nextInt();

        for (int i = 0; i < nu; i++) {

            System.out.println("----------------------------------------");
            System.out.println("Digite el numero del cedula del cliente:");
            System.out.println("----------------------------------------");
            cli.setCedula(n.nextInt());
            em.setCedula_cliente(cli.getCedula());

            System.out.println("-----------------------------");
            System.out.println("Digite el nombre del cliente:");
            System.out.println("-----------------------------");
            cli.setNombre(t.nextLine());
            em.setNombre_cliente(cli.getNombre());

            System.out.println("-------------------------------------------------");
            System.out.println("Digite el tipo de cliente (Unitario o Mayorista):");
            System.out.println("-------------------------------------------------");
            cli.setTipo(t.nextLine());
            em.setTipo_cliente(cli.getTipo());
            
            em.setNumero_cliente("cliente#"+String.valueOf(r.nextInt(1000)));
            em.Ver_Clientes();

        }
        
        

    }

    public static void main(String[] args) {
        Usuario();
    }

}
