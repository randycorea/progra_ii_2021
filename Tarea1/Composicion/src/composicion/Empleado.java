package composicion;

public class Empleado {
    
    //Definicion de Variables a Utilizar
    private String cedula;
    private String nombre;
    private String tipo_de_empleado;
    
    //Metodo Constructor
    public Empleado(){
        
    }
    
    //Metodos para accesos directo a los atributos
    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo_de_empleado() {
        return tipo_de_empleado;
    }

    public void setTipo_de_empleado(String tipo_de_empleado) {
        this.tipo_de_empleado = tipo_de_empleado;
    }
    
    
    
}
