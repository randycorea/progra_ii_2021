package composicion;

import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Empresa {

    //Definicion de Variables a utilizar
    private ArrayList<Empleado> empleados = new ArrayList<Empleado>();

    //Metodo Constructor
    public Empresa() {

    }

    public void Consultar() {

        if (empleados == null) {
            JOptionPane.showMessageDialog(null, "Error no hay datos en el Sistema", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            
            int ne=1;
            
            System.out.println("--Lista de Empleados--");
            
            for (Empleado o : empleados) {
                
                System.out.println("----------------");
                System.out.println("Empleado #" + String.valueOf(ne));
                System.out.println("----------------");
                
                System.out.println("");
                
                System.out.println("--Cédula del Empleado--");
                System.out.println(o.getCedula());
                System.out.println("-----------------------");
                
                System.out.println("");
                
                System.out.println("--Nombre del Empleado--");
                System.out.println(o.getNombre());
                System.out.println("-----------------------");
                
                System.out.println("");
                
                System.out.println("--Tipo de Empleado--");
                System.out.println(o.getTipo_de_empleado());
                System.out.println("--------------------");
                
                System.out.println("");
                
                ne++;
                
            }
        }
    }

    //Metodo para Agregar Empleados
    public void Agregar_Empleado() {

        Scanner t = new Scanner(System.in);
        Empleado empleado = new Empleado();

        System.out.println("-----------------------------------------");
        System.out.println("Digite el número de cedula del empleado: ");
        System.out.println("-----------------------------------------");
        empleado.setCedula(t.nextLine());
        
        System.out.println("");

        System.out.println("----------------------------------------");
        System.out.println("Digite el nombre completo del empleado: ");
        System.out.println("----------------------------------------");
        empleado.setNombre(t.nextLine());
        
        System.out.println("");

        System.out.println("-----------------------------");
        System.out.println("Digite el tipo del empleado: ");
        System.out.println("-----------------------------");
        empleado.setTipo_de_empleado(t.nextLine());
        
        System.out.println("");

        System.out.println("Empleado Agregado Exitosamente");
        
        System.out.println("");

        //Agregar Empleado a Lista Previamente Creada de Empleados de La Empresa
        this.empleados.add(empleado);

    }

    //Metodos para accesos directo a los atributos
    public ArrayList<Empleado> getEmpleados() {
        return empleados;
    }

}
