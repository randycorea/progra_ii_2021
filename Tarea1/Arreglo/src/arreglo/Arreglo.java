package arreglo;

import java.util.Arrays;
import java.util.Scanner;

public class Arreglo {

    //Metodo en el cual se realiza la gestión de los salarios
    public static void Salarios() {

        //Definicion del Arreglo con el cual se va a trabajar.
        int[] salarios = new int[10];
        //Definicion de Variables a Utilizar
        int salario_mayor = salarios[0];
        int salario_menor = 100000000;
        int suma_salarios = 0;
        int promedio = 0;

        Scanner n = new Scanner(System.in);

        for (int i = 0; i < 10; i++) {

            System.out.println("--------------------------------");
            System.out.println("Digite el salario del empleado: ");
            System.out.println("--------------------------------");
            int salario = n.nextInt();

            System.out.println("");

            //Instruccion por lo cual se añade el salario al arreglo
            salarios[i] = salario;

        }

        for (int i = 0; i < 10; i++) {

            suma_salarios += salarios[i];

            //Condicion para encontrar el salario mas alto
            if (salarios[i] > salario_mayor) {
                salario_mayor = salarios[i];
            }

            //Condicion pare encontrar el salario mas bajo
            if (salarios[i] < salario_menor) {
                salario_menor = salarios[i];
            }

        }

        //Instruccion para sacar el promedio de todos los salarios
        promedio = suma_salarios / salarios.length;

        System.out.println("--------------------------------");
        System.out.println("Salario mas alto: " + String.valueOf(salario_mayor));
        System.out.println("--------------------------------");

        System.out.println("");

        System.out.println("--------------------------------");
        System.out.println("Salario mas bajo: " + String.valueOf(salario_menor));
        System.out.println("--------------------------------");

        System.out.println("");

        System.out.println("--------------------------------");
        System.out.println("Promedio de los Salarios: " + String.valueOf(promedio));
        System.out.println("--------------------------------");

    }

    public static void main(String[] args) {
        Salarios();
    }

}
