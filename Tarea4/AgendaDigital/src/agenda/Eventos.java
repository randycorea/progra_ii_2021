package agenda;

import java.util.ArrayList;

public abstract class Eventos {
    
    private String descripcion;
    private String hora;
    private String lugar;
    private String fecha;
    
    public Eventos(){
        
    }
    
    public abstract boolean Agregar_Evento();

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    
    
}
