package agenda;

import static vista.Main.lista_evento_tarea;

public class Evento_Tarea extends Eventos {

    private String curso;

    public Evento_Tarea() {

    }

    @Override
    public boolean Agregar_Evento() {
        lista_evento_tarea.add(this.getCurso() + "@" + this.getDescripcion() + "@"
                + this.getLugar() + "@" + this.getFecha() + "@" + this.getHora());

        System.out.println(lista_evento_tarea);

        if (lista_evento_tarea.isEmpty() == false) {
            return true;
        } else {
            return false;
        }
    }


    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

}
