package logica;

import agenda.Evento_Reunion;
import agenda.Evento_Tarea;

public class Logica_Eventos {

    Evento_Reunion reunion = new Evento_Reunion();
    Evento_Tarea tarea = new Evento_Tarea();

    public boolean agregar_tarea(String no, String d, String i, String fe, String he) {
        this.tarea.setCurso(no);
        this.tarea.setDescripcion(d);
        this.tarea.setLugar(i);
        this.tarea.setFecha(fe);
        this.tarea.setHora(he);

        return this.tarea.Agregar_Evento();
    }

    public boolean agregar_reunion(String tr, String d, String i, String fe, String he) {
        this.reunion.setTipo_de_reunion(tr);
        this.reunion.setDescripcion(d);
        this.reunion.setLugar(i);
        this.reunion.setFecha(fe);
        this.reunion.setHora(he);

        return this.reunion.Agregar_Evento();
    }

}
