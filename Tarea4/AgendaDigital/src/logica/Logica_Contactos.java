package logica;

import agenda.*;

public class Logica_Contactos {

    Contacto_Familiar contacto_familiar = new Contacto_Familiar();
    Contacto_Empresarial contacto_empresarial = new Contacto_Empresarial();
    Contacto_Amigo contacto_amigo = new Contacto_Amigo();

    public boolean agregar_contacto_familiar(String no, String ap, String num,
            String fn, String di, String ce, String pa) {

        this.contacto_familiar.setNombre(no);
        this.contacto_familiar.setApellidos(ap);
        this.contacto_familiar.setTelefono(num);
        this.contacto_familiar.setFecha_nacimiento(fn);
        this.contacto_familiar.setDireccion(di);
        this.contacto_familiar.setCorreo_electronico(ce);
        this.contacto_familiar.setParentesco(pa);

        return contacto_familiar.Agregar_Contacto();

    }
    

    public boolean agregar_contacto_empresarial(String no, String ap, String num,
            String fn, String di, String ce, String ne) {

        this.contacto_empresarial.setNombre(no);
        this.contacto_empresarial.setApellidos(ap);
        this.contacto_empresarial.setTelefono(num);
        this.contacto_empresarial.setFecha_nacimiento(fn);
        this.contacto_empresarial.setDireccion(di);
        this.contacto_empresarial.setCorreo_electronico(ce);
        this.contacto_empresarial.setNombre_empresa(ne);

        return contacto_empresarial.Agregar_Contacto();

    }

    public boolean agregar_contacto_amistad(String no, String ap, String num,
            String fn, String di, String ce, String lc) {

        this.contacto_amigo.setNombre(no);
        this.contacto_amigo.setApellidos(ap);
        this.contacto_amigo.setTelefono(num);
        this.contacto_amigo.setFecha_nacimiento(fn);
        this.contacto_amigo.setDireccion(di);
        this.contacto_amigo.setCorreo_electronico(ce);
        this.contacto_amigo.setLugar_de_conocimiento(lc);

        return contacto_amigo.Agregar_Contacto();

    }

}
