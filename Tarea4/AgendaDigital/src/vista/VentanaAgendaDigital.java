package vista;

public class VentanaAgendaDigital extends javax.swing.JFrame {

    Añadir_Contacto_Familiar vacf = new Añadir_Contacto_Familiar();
    Añadir_Contacto_Empresarial vace = new Añadir_Contacto_Empresarial();
    Añadir_Contacto_Amistad vaca = new Añadir_Contacto_Amistad();
    Añadir_Evento_Tarea vaet = new Añadir_Evento_Tarea();
    Añadir_Evento_Reunion vaer = new Añadir_Evento_Reunion();
    Mostrar_Contactos_Familiares vmcf = new Mostrar_Contactos_Familiares();
    Mostrar_Contactos_Empresariales vmce = new Mostrar_Contactos_Empresariales();
    Mostrar_Eventos_Tareas vmet = new Mostrar_Eventos_Tareas();
    Mostrar_Eventos_Reuniones vmer = new Mostrar_Eventos_Reuniones();

    public VentanaAgendaDigital() {
        initComponents();
        setTitle("Agenda Digital");
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        escritorio = new javax.swing.JDesktopPane();
        barra_de_menu = new javax.swing.JMenuBar();
        menu_contactos = new javax.swing.JMenu();
        submenu_contactos = new javax.swing.JMenu();
        agregar_contacto_familiar = new javax.swing.JMenuItem();
        agregar_contacto_empresarial = new javax.swing.JMenuItem();
        agregar_contacto_amistad = new javax.swing.JMenuItem();
        mostrar_contactos = new javax.swing.JMenu();
        mostrar_contactos_familiares = new javax.swing.JMenuItem();
        mostrar_contactos_empresariales = new javax.swing.JMenuItem();
        mostrar_contactos_amistades = new javax.swing.JMenuItem();
        menu_eventos = new javax.swing.JMenu();
        submenu_eventos = new javax.swing.JMenu();
        agregar_evento_reunion = new javax.swing.JMenuItem();
        agregar_evento_tarea = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        mostrar_tareas = new javax.swing.JMenuItem();
        mostrar_reuniones = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        menu_contactos.setText("Contactos");

        submenu_contactos.setText("Agregar Contactos");

        agregar_contacto_familiar.setText("Contacto Familiar");
        agregar_contacto_familiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregar_contacto_familiarActionPerformed(evt);
            }
        });
        submenu_contactos.add(agregar_contacto_familiar);

        agregar_contacto_empresarial.setText("Contacto Empresarial");
        agregar_contacto_empresarial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregar_contacto_empresarialActionPerformed(evt);
            }
        });
        submenu_contactos.add(agregar_contacto_empresarial);

        agregar_contacto_amistad.setText("Contacto Amistad");
        agregar_contacto_amistad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregar_contacto_amistadActionPerformed(evt);
            }
        });
        submenu_contactos.add(agregar_contacto_amistad);

        menu_contactos.add(submenu_contactos);

        mostrar_contactos.setText("Mostrar Contactos");

        mostrar_contactos_familiares.setText("Mostrar Contactos de Familiares");
        mostrar_contactos_familiares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrar_contactos_familiaresActionPerformed(evt);
            }
        });
        mostrar_contactos.add(mostrar_contactos_familiares);

        mostrar_contactos_empresariales.setText("Mostrar Contactos Empresariales");
        mostrar_contactos_empresariales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrar_contactos_empresarialesActionPerformed(evt);
            }
        });
        mostrar_contactos.add(mostrar_contactos_empresariales);

        mostrar_contactos_amistades.setText("Mostrar Contactos de Amistades");
        mostrar_contactos.add(mostrar_contactos_amistades);

        menu_contactos.add(mostrar_contactos);

        barra_de_menu.add(menu_contactos);

        menu_eventos.setText("Eventos");

        submenu_eventos.setText("Agregar Eventos");

        agregar_evento_reunion.setText("Agregar Reunion");
        agregar_evento_reunion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregar_evento_reunionActionPerformed(evt);
            }
        });
        submenu_eventos.add(agregar_evento_reunion);

        agregar_evento_tarea.setText("Agregar Tarea");
        agregar_evento_tarea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregar_evento_tareaActionPerformed(evt);
            }
        });
        submenu_eventos.add(agregar_evento_tarea);

        menu_eventos.add(submenu_eventos);

        jMenu1.setText("Mostrar Eventos");

        mostrar_tareas.setText("Mostrar Tareas");
        mostrar_tareas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrar_tareasActionPerformed(evt);
            }
        });
        jMenu1.add(mostrar_tareas);

        mostrar_reuniones.setText("Mostrar Reuniones");
        mostrar_reuniones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrar_reunionesActionPerformed(evt);
            }
        });
        jMenu1.add(mostrar_reuniones);

        menu_eventos.add(jMenu1);

        barra_de_menu.add(menu_eventos);

        setJMenuBar(barra_de_menu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(escritorio, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 709, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(escritorio, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 440, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void agregar_contacto_familiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregar_contacto_familiarActionPerformed
        this.escritorio.add(vacf);
        vacf.setVisible(true);
    }//GEN-LAST:event_agregar_contacto_familiarActionPerformed

    private void agregar_contacto_empresarialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregar_contacto_empresarialActionPerformed
        escritorio.add(vace);
        vace.setVisible(true);
    }//GEN-LAST:event_agregar_contacto_empresarialActionPerformed

    private void agregar_contacto_amistadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregar_contacto_amistadActionPerformed
        escritorio.add(vaca);
        vaca.setVisible(true);
    }//GEN-LAST:event_agregar_contacto_amistadActionPerformed

    private void agregar_evento_tareaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregar_evento_tareaActionPerformed
        escritorio.add(vaet);
        vaet.setVisible(true);
    }//GEN-LAST:event_agregar_evento_tareaActionPerformed

    private void agregar_evento_reunionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregar_evento_reunionActionPerformed
        escritorio.add(vaer);
        vaer.setVisible(true);
    }//GEN-LAST:event_agregar_evento_reunionActionPerformed

    private void mostrar_contactos_familiaresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostrar_contactos_familiaresActionPerformed
        escritorio.add(vmcf);
        vmcf.setVisible(true);
    }//GEN-LAST:event_mostrar_contactos_familiaresActionPerformed

    private void mostrar_contactos_empresarialesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostrar_contactos_empresarialesActionPerformed
        escritorio.add(vmce);
        vmce.setVisible(true);
    }//GEN-LAST:event_mostrar_contactos_empresarialesActionPerformed

    private void mostrar_tareasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostrar_tareasActionPerformed
        escritorio.add(vmet);
        vmet.setVisible(true);
    }//GEN-LAST:event_mostrar_tareasActionPerformed

    private void mostrar_reunionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostrar_reunionesActionPerformed
        escritorio.add(vmer);
        vmer.setVisible(true);
    }//GEN-LAST:event_mostrar_reunionesActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaAgendaDigital.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaAgendaDigital.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaAgendaDigital.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaAgendaDigital.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaAgendaDigital().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem agregar_contacto_amistad;
    private javax.swing.JMenuItem agregar_contacto_empresarial;
    private javax.swing.JMenuItem agregar_contacto_familiar;
    private javax.swing.JMenuItem agregar_evento_reunion;
    private javax.swing.JMenuItem agregar_evento_tarea;
    private javax.swing.JMenuBar barra_de_menu;
    private javax.swing.JDesktopPane escritorio;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu menu_contactos;
    private javax.swing.JMenu menu_eventos;
    private javax.swing.JMenu mostrar_contactos;
    private javax.swing.JMenuItem mostrar_contactos_amistades;
    private javax.swing.JMenuItem mostrar_contactos_empresariales;
    private javax.swing.JMenuItem mostrar_contactos_familiares;
    private javax.swing.JMenuItem mostrar_reuniones;
    private javax.swing.JMenuItem mostrar_tareas;
    private javax.swing.JMenu submenu_contactos;
    private javax.swing.JMenu submenu_eventos;
    // End of variables declaration//GEN-END:variables
}
