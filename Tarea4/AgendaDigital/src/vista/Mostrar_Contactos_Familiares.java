package vista;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import agenda.Contacto_Familiar;
import static vista.Main.lista_contacto_familiar;

public class Mostrar_Contactos_Familiares extends javax.swing.JInternalFrame {
    
    Contacto_Familiar lc = new Contacto_Familiar();
    
    public Mostrar_Contactos_Familiares() {
        initComponents();
        setTitle("Contactos Familiares");
    }

    public void Mostrar_Contactos() {
        
        if (lista_contacto_familiar.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Error, No hay Datos Disponibles en el Sistema", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            int t = lista_contacto_familiar.size();
            String nombreC[] = {"Nombre", "Apellidos", "Telefono","Fecha Nacimiento", 
                "Dirección", "Correo Electronico", "Parentesco"};
            String datos[][] = new String[t][7];
            String[] separar = new String[7];
            for (int i = 0; i < lista_contacto_familiar.size(); i++) {
                separar = lista_contacto_familiar.get(i).split("#");
                datos[i][0] = separar[0];
                datos[i][1] = separar[1];
                datos[i][2] = separar[2];
                datos[i][3] = separar[3];
                datos[i][4] = separar[4];
                datos[i][5] = separar[5];
                datos[i][6] = separar[6];
            }
            this.contactos_familiares.setModel(new DefaultTableModel(datos, nombreC));
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        contactos_familiares = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        Mostrar = new javax.swing.JButton();

        setClosable(true);

        jPanel1.setBackground(new java.awt.Color(115, 115, 115));

        contactos_familiares.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(contactos_familiares);

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(254, 254, 254));
        jLabel1.setText("Contactos Familiares");

        Mostrar.setText("Mostrar Contactos");
        Mostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MostrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(318, 318, 318)
                                .addComponent(Mostrar))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(289, 289, 289)
                                .addComponent(jLabel1)))
                        .addGap(0, 288, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Mostrar)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void MostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MostrarActionPerformed
        //ArrayList<String> c = .Mostrar_Contactos();
        Mostrar_Contactos();
    }//GEN-LAST:event_MostrarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Mostrar;
    private javax.swing.JTable contactos_familiares;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
