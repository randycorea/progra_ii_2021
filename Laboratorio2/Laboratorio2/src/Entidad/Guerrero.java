/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author efren
 */
public class Guerrero extends Personaje{
    
    private String arma;

    
    public Guerrero()
    {
        super();
        /*super.setNombre("Yone");
        super.setEnergia(1000);
        this.setArma("Espada");*/
    }
    
    @Override
    public String Alimentarse(int alimento)
    {
        this.setEnergia(this.getEnergia()+alimento+2);
        return String.format("El personaje s% tiene de nivel de energia d%",
                this.getNombre(),this.getEnergia());
    }
    
    
    public String Combatir(int energia){
        super.setEnergia(super.getEnergia()-energia);  
        return this.getArma()+String.valueOf(energia);      
    }
    

    public String getArma() {
        return arma;
    }

    public void setArma(String arma) {
        this.arma = arma;
    }
}
