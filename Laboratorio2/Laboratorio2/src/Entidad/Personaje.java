/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author efren
 */
public class Personaje {
    
    private String nombre;
    private int energia; 
    
    public Personaje()
    {
        
    }
    
    public String Alimentarse(int alimento)
    {
        this.setEnergia(this.getEnergia()+alimento);
        return String.format("El personaje s% tiene de nivel de energia d%",
                this.getNombre(),this.getEnergia());
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the energia
     */
    public int getEnergia() {
        return energia;
    }

    /**
     * @param energia the energia to set
     */
    public void setEnergia(int energia) {
        this.energia = energia;
    }
    
               
    
}
