/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author efren
 */
public class Mago extends Personaje{

    private String poder;
    
    public Mago()
    {
        super();
        super.setEnergia(100);
    }
    
    public int Encantar()
    {
        super.setEnergia(this.getEnergia()-2);
        return super.getEnergia();
    }
    
    @Override
    public String Alimentarse(int alimento)
    {
        this.setEnergia(this.getEnergia()+alimento+2);
        return String.format("El personaje s% tiene de nivel de energia d%",
                this.getNombre(),this.getEnergia());
    }
    
    /**
     * @return the poder
     */
    public String getPoder() {
        return poder;
    }

    /**
     * @param poder the poder to set
     */
    public void setPoder(String poder) {
        this.poder = poder;
    }
    
}
