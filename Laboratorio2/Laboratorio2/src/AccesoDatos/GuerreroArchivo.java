package AccesoDatos;

import Entidad.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GuerreroArchivo extends Archivo<Guerrero> {

    private static String path;
    private static BufferedWriter escribir;
    private static BufferedReader leer;

    public GuerreroArchivo() {
        try {
            path = new File("src/Datos/Guerreros.txt").getCanonicalPath();
        } catch (IOException ex) {
            Logger.getLogger(GuerreroArchivo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void CrearArchivo(String nombreArchivo, Boolean agregar) throws IOException {
        File archivo = new File(nombreArchivo);
        if (archivo.exists() && !archivo.isFile()) {
            throw new IOException(archivo.getName() + " no es un archivo");
        }
        escribir = new BufferedWriter(new FileWriter(archivo, agregar));
    }

    @Override
    public void CerrarArchivo() throws IOException {
        escribir.close();
    }

    @Override
    public Boolean Existe(Guerrero object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean Agregar(Guerrero object) {
        try {
            this.CrearArchivo(this.path, true);
                escribir.write(String.valueOf(object.getNombre()) + "@");
                escribir.write(object.getEnergia()+ "@");
                escribir.write(object.getArma());
                escribir.write(System.lineSeparator());
                this.CerrarArchivo();
                return true;
            
        } catch (IOException ex) {
            Logger.getLogger(GuerreroArchivo.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }

    @Override
    public int Autoincremental() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean Editar(Guerrero object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();
            while ((line = leer.readLine()) != null) {
                String[] data = line.split("@");
                if (data[0].equals(String.valueOf(object.getNombre()))) {
                    cadena.append(String.valueOf(object.getNombre()) + "@"
                            + object.getEnergia()+ "@"
                            + object.getArma()+ System.lineSeparator());
                } else {
                    cadena.append(line + System.lineSeparator());
                }
            }
            leer.close();
            this.CrearArchivo(this.path, false);
            this.CrearArchivo(this.path, true);
            escribir.write(cadena.toString());
            this.CerrarArchivo();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GuerreroArchivo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GuerreroArchivo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public Boolean Eliminar(Guerrero object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();
            while ((line = leer.readLine()) != null) {
                String[] data = line.split("@");
                if (data[0].equals(String.valueOf(object.getNombre()))) {
                    cadena.append(line
                            + System.lineSeparator());
                }
            }
            leer.close();
            this.CrearArchivo(this.path, false);
            this.CrearArchivo(this.path, true);
            escribir.write(cadena.toString());
            this.CerrarArchivo();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GuerreroArchivo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GuerreroArchivo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public List<Guerrero> Obtener() {
        List<Guerrero> guerreros=new ArrayList();
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();
            while ((line = leer.readLine()) != null) {
                String[] data = line.split("@");
                Guerrero guerrero=new Guerrero();
                guerrero.setNombre(data[0]);
                guerrero.setEnergia(Integer.parseInt(data[1]));
                guerrero.setArma(data[2]);
                guerreros.add(guerrero);
            }
            leer.close();

            return guerreros;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GuerreroArchivo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GuerreroArchivo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return guerreros;
    }

}
