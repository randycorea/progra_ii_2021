/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccesoDatos;

import Entidad.Mago;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author efren
 */
public class MagoArchivo extends Archivo<Mago>{
    
    private static String path;
    private static BufferedWriter escribir;
    private static BufferedReader leer;
    
    public MagoArchivo() {
        try {
            path = new File("src/Datos/Magos.txt").getCanonicalPath();
        } catch (IOException ex) {
            Logger.getLogger(MagoArchivo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void CrearArchivo(String nombreArchivo, Boolean agregar) throws IOException {
        File archivo = new File(nombreArchivo);
        if (archivo.exists() && !archivo.isFile()) {
            throw new IOException(archivo.getName() + " no es un archivo");
        }
        escribir = new BufferedWriter(new FileWriter(archivo, agregar));
    }

    @Override
    public void CerrarArchivo() throws IOException {
        escribir.close();
    }

    @Override
    public Boolean Existe(Mago object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean Agregar(Mago object) {
        try {
            this.CrearArchivo(this.path, true);
                escribir.write(String.valueOf(object.getNombre()) + "@");
                escribir.write(object.getEnergia()+ "@");
                escribir.write(object.getPoder());
                escribir.write(System.lineSeparator());
                this.CerrarArchivo();
                return true;
            
        } catch (IOException ex) {
            Logger.getLogger(MagoArchivo.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public Boolean Editar(Mago object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();
            while ((line = leer.readLine()) != null) {
                String[] data = line.split("@");
                if (data[0].equals(String.valueOf(object.getNombre()))) {
                    cadena.append(String.valueOf(object.getNombre()) + "@"
                            + object.getEnergia()+ "@"
                            + object.getPoder()+ System.lineSeparator());
                } else {
                    cadena.append(line + System.lineSeparator());
                }
            }
            leer.close();
            this.CrearArchivo(this.path, false);
            this.CrearArchivo(this.path, true);
            escribir.write(cadena.toString());
            this.CerrarArchivo();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MagoArchivo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MagoArchivo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public Boolean Eliminar(Mago object) {
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();
            while ((line = leer.readLine()) != null) {
                String[] data = line.split("@");
                if (!data[0].equals(String.valueOf(object.getNombre()))) {
                    cadena.append(line
                            + System.lineSeparator());
                }
            }
            leer.close();
            this.CrearArchivo(this.path, false);
            this.CrearArchivo(this.path, true);
            escribir.write(cadena.toString());
            this.CerrarArchivo();
            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MagoArchivo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MagoArchivo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public List<Mago> Obtener() {
        List<Mago> magos=new ArrayList();
        try {
            leer = new BufferedReader(new FileReader(this.path));
            String line = null;
            StringBuilder cadena = new StringBuilder();
            while ((line = leer.readLine()) != null) {
                String[] data = line.split("@");
                Mago mago=new Mago();
                mago.setNombre(data[0]);
                mago.setEnergia(Integer.parseInt(data[1]));
                mago.setPoder(data[2]);
                magos.add(mago);
            }
            leer.close();

            return magos;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MagoArchivo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MagoArchivo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return magos;
    }

    @Override
    public int Autoincremental() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
}
