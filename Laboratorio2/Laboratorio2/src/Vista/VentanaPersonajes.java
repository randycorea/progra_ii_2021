package Vista;

import AccesoDatos.GuerreroArchivo;
import AccesoDatos.MagoArchivo;
import Entidad.Guerrero;
import Entidad.Mago;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;

public class VentanaPersonajes extends javax.swing.JFrame {

    AñadirGuerrero vag = new AñadirGuerrero();
    AñadirMagos vam = new AñadirMagos();
    EliminarGuerrero veg = new EliminarGuerrero();
    GuerreroArchivo ach_g = new GuerreroArchivo();
    MagoArchivo arc_m = new MagoArchivo();
    List<Guerrero> g = new ArrayList();
    List<Mago> m = new ArrayList();
    DefaultListModel modelo = new DefaultListModel();

    public VentanaPersonajes() {
        initComponents();
        setTitle("Personajes");
        setLocationRelativeTo(null);
        this.vista.setVisible(false);
    }

    public void MostrarGuerreros() {

        vista.setModel(modelo);
        this.vista.setVisible(true);
        this.vista.setEnabled(true);
        g = ach_g.Obtener();

        modelo.clear();
        for (Guerrero str : g) {
            modelo.addElement(str.getNombre() + "-" + str.getArma() + "-" + str.getEnergia());
        }
    }

    public void MostrarMagos() {
        vista.setModel(modelo);
        this.vista.setVisible(true);
        this.vista.setEnabled(true);
        m = arc_m.Obtener();
        
        modelo.clear();
        for (Mago str : m) {
            modelo.addElement(str.getNombre() + "-" + str.getPoder() + "-" + str.getEnergia());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        vista = new javax.swing.JList<>();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        AñadirMagos = new javax.swing.JMenuItem();
        ModificarMagos = new javax.swing.JMenuItem();
        EliminarMagos = new javax.swing.JMenuItem();
        MostrarMagos = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        AñadirGuerreros = new javax.swing.JMenuItem();
        ModificarGuerreros = new javax.swing.JMenuItem();
        EliminarGuerreros = new javax.swing.JMenuItem();
        MostrarGuerreros = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jScrollPane2.setViewportView(vista);

        jMenu1.setText("Magos");

        AñadirMagos.setText("Añadir");
        AñadirMagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AñadirMagosActionPerformed(evt);
            }
        });
        jMenu1.add(AñadirMagos);

        ModificarMagos.setText("Modificar");
        jMenu1.add(ModificarMagos);

        EliminarMagos.setText("Eliminar");
        jMenu1.add(EliminarMagos);

        MostrarMagos.setText("Mostrar");
        MostrarMagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MostrarMagosActionPerformed(evt);
            }
        });
        jMenu1.add(MostrarMagos);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Guerreros");

        AñadirGuerreros.setText("Añadir");
        AñadirGuerreros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AñadirGuerrerosActionPerformed(evt);
            }
        });
        jMenu2.add(AñadirGuerreros);

        ModificarGuerreros.setText("Modificar");
        jMenu2.add(ModificarGuerreros);

        EliminarGuerreros.setText("Eliminar");
        EliminarGuerreros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarGuerrerosActionPerformed(evt);
            }
        });
        jMenu2.add(EliminarGuerreros);

        MostrarGuerreros.setText("Mostrar");
        MostrarGuerreros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MostrarGuerrerosActionPerformed(evt);
            }
        });
        jMenu2.add(MostrarGuerreros);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(59, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AñadirGuerrerosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AñadirGuerrerosActionPerformed
        vag.setVisible(true);
        modelo.clear();
    }//GEN-LAST:event_AñadirGuerrerosActionPerformed

    private void MostrarGuerrerosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MostrarGuerrerosActionPerformed
        MostrarGuerreros();
    }//GEN-LAST:event_MostrarGuerrerosActionPerformed

    private void EliminarGuerrerosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarGuerrerosActionPerformed
        veg.setVisible(true);
        modelo.clear();
    }//GEN-LAST:event_EliminarGuerrerosActionPerformed

    private void AñadirMagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AñadirMagosActionPerformed
        vam.setVisible(true);
    }//GEN-LAST:event_AñadirMagosActionPerformed

    private void MostrarMagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MostrarMagosActionPerformed
        MostrarMagos();
    }//GEN-LAST:event_MostrarMagosActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaPersonajes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaPersonajes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaPersonajes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaPersonajes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaPersonajes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem AñadirGuerreros;
    private javax.swing.JMenuItem AñadirMagos;
    private javax.swing.JMenuItem EliminarGuerreros;
    private javax.swing.JMenuItem EliminarMagos;
    private javax.swing.JMenuItem ModificarGuerreros;
    private javax.swing.JMenuItem ModificarMagos;
    private javax.swing.JMenuItem MostrarGuerreros;
    private javax.swing.JMenuItem MostrarMagos;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList<String> vista;
    // End of variables declaration//GEN-END:variables
}
