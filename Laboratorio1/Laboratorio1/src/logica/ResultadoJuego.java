package logica;

import juego.*;

public class ResultadoJuego {

    JuegoAdivinaNumero j = new JuegoAdivinaNumero();
    JuegoAdivinaNumeroPar j2 = new JuegoAdivinaNumeroPar();
    JuegoAdivinaNumeroImpar j3 = new JuegoAdivinaNumeroImpar();

    public boolean ResultadoJuegoAdivinaNumero(int numero) {

        j.setNumero(numero);
        boolean resultado = j.Juega();

        if (resultado == true) {
            return true;
        } else {
            return false;
        }

    }

    public boolean ResultadoJuegoAdivinaNumeroPar(int numero) {

        j2.setNumero(numero);
        boolean resultado = j2.Juega();

        if (resultado == true) {
            return true;
        } else {
            return false;
        }

    }

    public boolean ResultadoJuegoAdivinaNumeroImpar(int numero) {

        j3.setNumero(numero);
        boolean resultado = j3.Juega();

        if (resultado == true) {
            return true;
        } else {
            return false;
        }

    }

    public int VidasJuegoImpar() {
        return j3.getVida();
    }

    public int RecordJuegoImpar() {
        return j3.getRecord();
    }

    public boolean ReiniciarJuegoImpar() {
        return j3.ReiniciaPartida();
    }

    public int VidasJuegoPar() {
        return j2.getVida();
    }

    public int RecordJuegoPar() {
        return j2.getRecord();
    }

    public boolean ReiniciarJuegoPar() {
        return j2.ReiniciaPartida();
    }

    public int VidasJuego() {
        return j.getVida();
    }

    public int RecordJuego() {
        return j.getRecord();
    }

    public boolean ReiniciarJuego() {
        return j.ReiniciaPartida();
    }

}
