package juego;

import java.util.Random;

public class JuegoAdivinaNumeroImpar extends JuegoAdivinaNumero{
    
    private int vidas=5;
    private int numero;
    
    public JuegoAdivinaNumeroImpar(){
        super();
    }
    
        public int NumeroImpar() {
        Random r = new Random();
        int num_par = 0;
        int c = 0;
        while (c == 0) {
            int np = r.nextInt(9) + 1;
            if (np % 2 !=0) {
                num_par = np;
                c = 1;
            } else {
                c = 0;
            }
        }
        System.out.println(num_par);
        return num_par;
    }

    @Override
    public int ActualizarRecord() {
        this.setRecord(this.getRecord() + 1);
        return this.getRecord();
    }

    @Override
    public boolean Juega() {
        int np = NumeroImpar();
        if (this.getNumero() == np) {
            ActualizarRecord();
            return true;
        } else {
            QuitaVida();
            return false;
        }
    }

    @Override
    public int QuitaVida() {
        int vidamenos = this.getVida() - 1;
        this.setVida(vidamenos);
        return this.getVida();
    }

    @Override
    public boolean ReiniciaPartida() {
        if (this.getVida() == 0) {
            this.setRecord(0);
            this.setVida(5);
            return true;
        } else if (this.getVida() != 0) {
            return false;
        } else {
            return true;
        }
    }

    public int getVidas() {
        return vidas;
    }

    public void setVidas(int vidas) {
        this.vidas = vidas;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    
    
}
