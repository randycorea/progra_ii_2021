package juego;

import java.util.Random;

public class JuegoAdivinaNumero extends Juego {

    private int vida = 5;
    private int numero;
    private int record = 0;

    public JuegoAdivinaNumero() {

    }

    @Override
    public boolean Juega() {

        Random r = new Random();
        int np = r.nextInt(9) + 1;
        if (this.getNumero() == np) {
            ActualizarRecord();
            return true;
        } else {
            QuitaVida();
            return false;
        }
    }

    @Override
    public boolean ReiniciaPartida() {
        if (this.getVida() == 0) {
            this.setRecord(0);
            this.setVida(5);
            return true;
        } else if (this.getVida() != 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public int ActualizarRecord() {
        this.setRecord(this.getRecord() + 1);
        return this.getRecord();
    }

    @Override
    public int QuitaVida() {
        int vidamenos = this.getVida() - 1;
        this.setVida(vidamenos);
        return this.getVida();
    }

    public int getRecord() {
        return record;
    }

    public void setRecord(int record) {
        this.record = record;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

}
