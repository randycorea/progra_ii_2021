
package juego;

public abstract class Juego {
    
    public Juego(){
        
    }
    
    public abstract boolean Juega();
    public abstract boolean ReiniciaPartida();
    public abstract int ActualizarRecord();
    public abstract int QuitaVida();
    
}
