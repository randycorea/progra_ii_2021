package presentacion;

import javax.swing.JOptionPane;
import logica.ResultadoJuego;

public class VentanaJuegoAdivinaNumerosImpares extends javax.swing.JFrame {

    ResultadoJuego r = new ResultadoJuego();

    public VentanaJuegoAdivinaNumerosImpares() {
        initComponents();
        setTitle("Adivina Numeros Impares");
        setLocationRelativeTo(null);
        this.label_numero_vidas.setText("5");
        this.label_record_numero.setText("0");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        label_titulo = new javax.swing.JLabel();
        campo_texto_numero = new javax.swing.JTextField();
        boton_adivinar = new javax.swing.JButton();
        label_record = new javax.swing.JLabel();
        label_numero_vidas = new javax.swing.JLabel();
        label_vidas = new javax.swing.JLabel();
        label_record_numero = new javax.swing.JLabel();
        boton_salir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(46, 110, 254));

        label_titulo.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        label_titulo.setForeground(new java.awt.Color(254, 254, 254));
        label_titulo.setText("Adivina un numero entre 0 y 10");

        boton_adivinar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/boton.png"))); // NOI18N
        boton_adivinar.setText("Jugar");
        boton_adivinar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_adivinarActionPerformed(evt);
            }
        });

        label_record.setForeground(new java.awt.Color(254, 254, 254));
        label_record.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/record.png"))); // NOI18N
        label_record.setText("Record:");

        label_numero_vidas.setForeground(new java.awt.Color(254, 254, 254));

        label_vidas.setForeground(new java.awt.Color(254, 254, 254));
        label_vidas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/vidas.png"))); // NOI18N
        label_vidas.setText("Vidas: ");

        label_record_numero.setForeground(new java.awt.Color(254, 254, 254));

        boton_salir.setText("Salir");
        boton_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_salirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(label_record)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label_record_numero, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(label_vidas)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(label_numero_vidas, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(label_titulo)
                        .addContainerGap(16, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(campo_texto_numero, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(79, 79, 79))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(boton_salir)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(boton_adivinar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label_record_numero, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(label_record, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(label_vidas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(label_numero_vidas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(18, 18, 18)
                .addComponent(label_titulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(campo_texto_numero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(boton_adivinar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(boton_salir, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void boton_adivinarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_adivinarActionPerformed

        int numero = Integer.parseInt(this.campo_texto_numero.getText());

        if (numero >= 0 && numero <= 10) {
            if (numero % 2 != 0) {
                boolean resultado = r.ResultadoJuegoAdivinaNumeroImpar(numero);
                this.campo_texto_numero.setText("");
                if (resultado == true) {
                    JOptionPane.showMessageDialog(this, "¡Acertaste!",
                            "Felicitaciones", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, "¡Fallaste!",
                            "Que Pena", JOptionPane.INFORMATION_MESSAGE);
                    if (r.ReiniciarJuegoImpar() == true) {
                        JOptionPane.showMessageDialog(this, "Lo Sentimos, te has quedado sin vidas, por ende perdiste",
                                "Lo Sentimos", JOptionPane.ERROR_MESSAGE);
                        this.campo_texto_numero.setText("");
                        this.label_numero_vidas.setText("5");
                        this.label_record_numero.setText("0");
                    }
                }
            } else {
                JOptionPane.showMessageDialog(this, "Error, debe digitar un numero impar entre 0 y 10",
                        "Error", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(this, "Error, debe digitar un numero entre 0 y 10",
                    "Error", JOptionPane.ERROR_MESSAGE);
            this.campo_texto_numero.setText("");
        }

        this.label_record_numero.setText(String.valueOf(r.RecordJuegoImpar()));
        this.label_numero_vidas.setText(String.valueOf(r.VidasJuegoImpar()));

    }//GEN-LAST:event_boton_adivinarActionPerformed

    private void boton_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_salirActionPerformed
        int c = JOptionPane.showConfirmDialog(this, "¿Desea salir del juego?", "Confirmación",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
        if (c == JOptionPane.OK_OPTION) {
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(this, "Esperamos que sigas disfrutando",
                    "Muchas Gracias", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_boton_salirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaJuegoAdivinaNumerosImpares.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaJuegoAdivinaNumerosImpares.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaJuegoAdivinaNumerosImpares.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaJuegoAdivinaNumerosImpares.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaJuegoAdivinaNumerosImpares().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boton_adivinar;
    private javax.swing.JButton boton_salir;
    private javax.swing.JTextField campo_texto_numero;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel label_numero_vidas;
    private javax.swing.JLabel label_record;
    private javax.swing.JLabel label_record_numero;
    private javax.swing.JLabel label_titulo;
    private javax.swing.JLabel label_vidas;
    // End of variables declaration//GEN-END:variables
}
