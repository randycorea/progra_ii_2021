package presentacion;

import javax.swing.JOptionPane;

public class VentanaJuegos extends javax.swing.JFrame {

    VentanaJuegoAdivinarNumero vj1 = new VentanaJuegoAdivinarNumero();
    VentanaJuegoAdivinaNumerosPares vj2 = new VentanaJuegoAdivinaNumerosPares();
    VentanaJuegoAdivinaNumerosImpares vj3 = new VentanaJuegoAdivinaNumerosImpares();

    public VentanaJuegos() {
        initComponents();
        setTitle("Juegos de Adivinar Numeros");
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel = new javax.swing.JPanel();
        barra_de_menus = new javax.swing.JMenuBar();
        menu_juego_1 = new javax.swing.JMenu();
        menu_juego_2 = new javax.swing.JMenu();
        menu_juego_3 = new javax.swing.JMenu();
        menu_salir = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        panel.setBackground(new java.awt.Color(0, 106, 255));

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 631, Short.MAX_VALUE)
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 283, Short.MAX_VALUE)
        );

        menu_juego_1.setText("Juego Adivina Numeros  ");
        menu_juego_1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu_juego_1MouseClicked(evt);
            }
        });
        barra_de_menus.add(menu_juego_1);

        menu_juego_2.setText("Juego Adivina Numero Pares  ");
        menu_juego_2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu_juego_2MouseClicked(evt);
            }
        });
        barra_de_menus.add(menu_juego_2);

        menu_juego_3.setText("Juego Adivina Numero Impares  ");
        menu_juego_3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu_juego_3MouseClicked(evt);
            }
        });
        barra_de_menus.add(menu_juego_3);

        menu_salir.setText("Salir");
        menu_salir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menu_salirMouseClicked(evt);
            }
        });
        barra_de_menus.add(menu_salir);

        setJMenuBar(barra_de_menus);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void menu_juego_1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu_juego_1MouseClicked
        vj1.setVisible(true);
    }//GEN-LAST:event_menu_juego_1MouseClicked

    private void menu_salirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu_salirMouseClicked
        int c = JOptionPane.showConfirmDialog(this, "¿Desea salir del juego?", "Confirmación",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
        if (c == JOptionPane.OK_OPTION) {
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(this, "Esperamos que sigas disfrutando",
                    "Muchas Gracias", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_menu_salirMouseClicked

    private void menu_juego_2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu_juego_2MouseClicked
        vj2.setVisible(true);
    }//GEN-LAST:event_menu_juego_2MouseClicked

    private void menu_juego_3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu_juego_3MouseClicked
        vj3.setVisible(true);
    }//GEN-LAST:event_menu_juego_3MouseClicked

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaJuegos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaJuegos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaJuegos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaJuegos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaJuegos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar barra_de_menus;
    private javax.swing.JMenu menu_juego_1;
    private javax.swing.JMenu menu_juego_2;
    private javax.swing.JMenu menu_juego_3;
    private javax.swing.JMenu menu_salir;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
