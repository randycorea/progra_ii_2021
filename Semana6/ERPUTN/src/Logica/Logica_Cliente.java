package Logica;

import Acceso_Datos.Conexion_BD;
import Entidad.Cliente;
import java.sql.Statement;

public class Logica_Cliente {

    boolean existe_error = false;
    String mensaje_error = "";

    public void Crear(Cliente cli) {
        Conexion_BD conexion = new Conexion_BD();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "INSERT INTO clientes ("
                    + "cedula" + ","
                    + "nombre" + ","
                    + "direccion" + ")"
                    + " VALUES "
                    + "("
                    + "'" + cli.getCedula() + "'" + ","
                    + "'" + cli.getNombre() + "'" + ","
                    + "'" + cli.getDireccion() + "'" + ")";
            ejecucion.execute(sql);
            ejecucion.close();
            //Siempre desconectarse del servidor de base de datos o se quedan sin trabajo
            // gracias =)
            conexion.Desconectar();
        } catch (Exception e) {
            this.setExiste_error(true);
            this.setMensaje_error(e.getMessage());
        }
    }

    public void Editar(Cliente cliente) {
        
        Conexion_BD conexion = new Conexion_BD();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "UPDATE clientes SET "
                    + "cedula" + "=" + "'" + cliente.getCedula() + "'" + ","
                    + "nombre" + "=" + "'" + cliente.getNombre() + "'" + ","
                    + "direccion" + "=" + "'" + cliente.getDireccion() + "'"
                    + " WHERE id" + "=" + cliente.getId();

            ejecucion.execute(sql);
            ejecucion.close();
            //Siempre desconectarse del servidor de base de datos o se quedan sin trabajo
            // gracias =)
            conexion.Desconectar();

        } catch (Exception e) {
            this.setExiste_error(true);
            this.setMensaje_error(e.getMessage());
        }
    }

    public boolean isExiste_error() {
        return existe_error;
    }

    public void setExiste_error(boolean existe_error) {
        this.existe_error = existe_error;
    }

    public String getMensaje_error() {
        return mensaje_error;
    }

    public void setMensaje_error(String mensaje_error) {
        this.mensaje_error = mensaje_error;
    }

}
