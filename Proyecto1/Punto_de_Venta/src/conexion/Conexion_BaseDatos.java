package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Conexion_BaseDatos {
    
    private static String db = "SISTEMA_INVENTARIOS";
    private static String login = "postgres";
    private static String password = "admin123";
    private static String server = "localhost";
    private static String puerto = "5432";
    private static String url = "jdbc:postgresql://";
    private static Boolean existeError = false;
    private static String mensajeError = "";
    private Connection connection = null;
    ImageIcon icon = new ImageIcon("src/multimedia/icono_salir.png");
    
        public Conexion_BaseDatos() {
        try {
            
            connection = DriverManager.getConnection(url + server + ":" + puerto + "/" + db, login, password);
            if (connection != null) {
                System.out.println("La conexion esta correcta");
            } else {
                existeError = true;
                System.out.println("La conexion esta cerrada");
                mensajeError = "La conexion esta cerrada";
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "SQL State: "+e.getSQLState(),e.getMessage()+"Error",JOptionPane.ERROR_MESSAGE, icon);
        } catch (Exception e) {
        }
    }

    public void Desconectar() {
        try {
            this.getConnection().close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e ,"Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    
}
