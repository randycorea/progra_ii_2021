package logica;

import conexion.Conexion_BaseDatos;
import entidades.Producto;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Logica_Producto {

    private boolean existeError = false;
    private String mensajeError = "";

    public boolean Agregrar_Producto(Producto pro) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        pro.setPrecio_total_productos(pro.getPrecio_unitario() * pro.getCantidad());
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "INSERT INTO Producto ("
                    + "Nombre" + ","
                    + "Codigo" + ","
                    + "Cantidad" + ","
                    + "Precio_Unitario" + ","
                    + "Precio_Total_X_Cantidad_Productos" + ")"
                    + " VALUES "
                    + "("
                    + "'" + pro.getNombre() + "'" + ","
                    + "'" + pro.getCodigo() + "'" + ","
                    + "'" + String.valueOf(pro.getCantidad()) + "'" + ","
                    + "'" + String.valueOf(pro.getPrecio_unitario()) + "'" + ","
                    + "'" + String.valueOf(pro.getPrecio_total_productos()) + "'" + ")";
            ejecucion.execute(sql);
            ejecucion.close();
            conexion.Desconectar();
            return true;
        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            return false;
        }
    }

    public ArrayList Obtener_Datos_Producto(Producto pro) {
        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        ArrayList<String> productos = new ArrayList<String>();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "SELECT * FROM Producto ORDER BY ID_Producto ASC";
            ResultSet queryResult = ejecucion.executeQuery(sql);
            while (queryResult.next()) {

                String id = queryResult.getString("ID_Producto");
                pro.setId(Integer.parseInt(id));

                String nombre = queryResult.getString("Nombre");
                pro.setNombre(nombre);

                String codigo = queryResult.getString("Codigo");
                pro.setCodigo(codigo);

                String cantidad = queryResult.getString("Cantidad");
                pro.setCantidad(Integer.parseInt(cantidad));

                String precio = queryResult.getString("Precio_Unitario");
                pro.setPrecio_unitario(Float.parseFloat(precio));

                String precio_total = queryResult.getString("Precio_Total_X_Cantidad_Productos");
                pro.setPrecio_total_productos(Float.parseFloat(precio_total));

                productos.add(pro.getId() + "#" + pro.getNombre() + "#" + pro.getCodigo() + "#" + pro.getCantidad()
                        + "#" + pro.getPrecio_unitario() + "#" + pro.getPrecio_total_productos());

            }
            ejecucion.close();
            conexion.Desconectar();

        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
        }
        return productos;
    }

    public String Buscar_Producto(Producto p) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        String producto = "";

        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "SELECT Nombre, Precio_Unitario , Cantidad FROM Producto WHERE Codigo=" + "'" + p.getCodigo() + "';";
            ResultSet queryResult = ejecucion.executeQuery(sql);
            while (queryResult.next()) {

                String nombre = queryResult.getString("Nombre");
                p.setNombre(nombre);

                String canti = queryResult.getString("Cantidad");

                String precio_unitario = queryResult.getString("Precio_Unitario");
                p.setPrecio_unitario(Float.parseFloat(precio_unitario));

                if (Integer.parseInt(canti) < p.getCantidad() || Integer.parseInt(canti) == 0) {
                    producto = "NS";
                } else {

                    producto = p.getCantidad() + "#" + p.getNombre() + "#" + p.getPrecio_unitario() + "#"
                            + p.getPrecio_unitario() * p.getCantidad();

                    Salida_Producto(p);
                }

            }
            ejecucion.close();
            conexion.Desconectar();

        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            producto = "Fracaso";
        }
        return producto;

    }

    public boolean Salida_Producto(Producto p) {
        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        int cantidad = p.getCantidad();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "SELECT * FROM Producto WHERE Codigo=" + "'" + p.getCodigo() + "';";
            ResultSet queryResult = ejecucion.executeQuery(sql);

            while (queryResult.next()) {

                String id = queryResult.getString("ID_Producto");
                p.setId(Integer.parseInt(id));

                String no = queryResult.getString("Nombre");
                p.setNombre(no);

                String co = queryResult.getString("Codigo");
                p.setCodigo(co);

                String cant = queryResult.getString("Cantidad");
                p.setCantidad(Integer.parseInt(cant));

                String pre = queryResult.getString("Precio_Unitario");
                p.setPrecio_unitario(Float.parseFloat(pre));

                String pre_to = queryResult.getString("Precio_Total_X_Cantidad_Productos");
                p.setPrecio_total_productos(Float.parseFloat(pre_to));

                p.setCantidad(p.getCantidad() - cantidad);
                p.setPrecio_total_productos(p.getPrecio_unitario() * p.getCantidad());

                Actualizar_Producto(p);

            }
            ejecucion.close();
            conexion.Desconectar();
            return true;

        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            return false;
        }
    }

    public boolean Actualizar_Producto(Producto p) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "UPDATE Producto SET "
                    + "ID_Producto=" + "'" + p.getId() + "'" + ","
                    + "Nombre=" + "'" + p.getNombre() + "'" + ","
                    + "Codigo=" + "'" + p.getCodigo() + "'" + ","
                    + "Cantidad=" + "'" + p.getCantidad() + "'" + ","
                    + "Precio_Unitario=" + "'" + p.getPrecio_unitario() + "'" + ","
                    + "Precio_Total_X_Cantidad_Productos=" + "'" + p.getPrecio_total_productos() + "'" + " "
                    + "WHERE ID_Producto=" + "'" + p.getId() + "'" + ";";
            ejecucion.execute(sql);
            ejecucion.close();
            conexion.Desconectar();
            return true;
        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            return false;
        }
    }

    public boolean Eliminar_Producto(Producto pro) {

        Conexion_BaseDatos conexion = new Conexion_BaseDatos();
        try {
            Statement ejecucion;
            ejecucion = conexion.getConnection().createStatement();
            String sql = "DELETE FROM Producto WHERE "
                    + "(ID_Producto=" + "'" + pro.getId() + "'" + ") AND "
                    + "(Nombre=" + "'" + pro.getNombre() + "'" + ") AND "
                    + "(Codigo=" + "'" + pro.getCodigo() + "'" + ") AND "
                    + "(Cantidad=" + "'" + pro.getCantidad() + "'" + ") AND "
                    + "(Precio_Unitario=" + "'" + pro.getPrecio_unitario() + "'" + ") AND "
                    + "(Precio_Total_X_Cantidad_Productos=" + "'" + pro.getPrecio_total_productos() + "'" + ");";
            ejecucion.execute(sql);
            ejecucion.close();
            conexion.Desconectar();
            return true;
        } catch (Exception e) {
            this.setExisteError(true);
            this.setMensajeError(e.getMessage());
            JOptionPane.showMessageDialog(null, e, mensajeError, JOptionPane.ERROR_MESSAGE);
            return false;
        }

    }

    public boolean isExisteError() {
        return existeError;
    }

    public void setExisteError(boolean existeError) {
        this.existeError = existeError;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

}
