package vista;

import entidades.Venta;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import logica.Logica_Venta;

public class Reporte_Ventas extends javax.swing.JInternalFrame {
    
    Logica_Venta lv = new Logica_Venta();
    ArrayList<String> lista_ventas = new ArrayList<String>();
    Venta ve = new Venta();
    ImageIcon icon = new ImageIcon("src/multimedia/icono_salir.png");

    public Reporte_Ventas() {
        initComponents();
        setTitle("Reporte de Ventas");
        Mostrar_Ventas();
    }
    
    
        public void Mostrar_Ventas() {

        lista_ventas = lv.Obtener_Usuario(ve);
        int t = lista_ventas.size();

        if (lista_ventas.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Error, No hay Datos Disponibles en el Sistema", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            String nombreC[] = {"Numero de Venta", "Fecha", "Hora", "Subtotal",
                "I.V.A", "Total"};
            String datos[][] = new String[t][6];
            String[] separar = new String[6];
            for (int i = 0; i < lista_ventas.size(); i++) {
                separar = lista_ventas.get(i).split("#");
                datos[i][0] = separar[0];
                datos[i][1] = separar[1];
                datos[i][2] = separar[2];
                datos[i][3] = separar[3];
                datos[i][4] = separar[4];
                datos[i][5] = separar[5];
            }
            this.ventas.setModel(new DefaultTableModel(datos, nombreC));
        }
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        ventas = new javax.swing.JTable();
        boton_actualizar = new javax.swing.JButton();
        boton_salir = new javax.swing.JButton();

        setBackground(new java.awt.Color(78, 78, 78));
        setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        ventas.setBackground(new java.awt.Color(144, 144, 144));
        ventas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        ventas.setEnabled(false);
        ventas.setRowHeight(20);
        ventas.setRowMargin(5);
        jScrollPane1.setViewportView(ventas);

        boton_actualizar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_actualizar.setForeground(new java.awt.Color(254, 254, 254));
        boton_actualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/boton_actualizar.png"))); // NOI18N
        boton_actualizar.setText("Actualizar");
        boton_actualizar.setBorderPainted(false);
        boton_actualizar.setContentAreaFilled(false);
        boton_actualizar.setDefaultCapable(false);
        boton_actualizar.setFocusPainted(false);
        boton_actualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_actualizarActionPerformed(evt);
            }
        });

        boton_salir.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_salir.setForeground(new java.awt.Color(254, 254, 254));
        boton_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_salir_ventana.png"))); // NOI18N
        boton_salir.setText("Salir");
        boton_salir.setBorderPainted(false);
        boton_salir.setContentAreaFilled(false);
        boton_salir.setDefaultCapable(false);
        boton_salir.setFocusPainted(false);
        boton_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_salirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 740, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(boton_actualizar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(boton_salir)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(boton_salir)
                    .addComponent(boton_actualizar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void boton_actualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_actualizarActionPerformed
        Mostrar_Ventas();
    }//GEN-LAST:event_boton_actualizarActionPerformed

    private void boton_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_salirActionPerformed

        int opcion = JOptionPane.showConfirmDialog(this, "¿Deseas cerrar el programa?", "Pregunta",
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, icon);
        if (opcion == JOptionPane.OK_OPTION) {
            this.dispose();
        }
    }//GEN-LAST:event_boton_salirActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boton_actualizar;
    private javax.swing.JButton boton_salir;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable ventas;
    // End of variables declaration//GEN-END:variables
}
