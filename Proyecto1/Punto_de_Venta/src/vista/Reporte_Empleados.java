package vista;

import entidades.Empleado;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import logica.Logica_Empleado;

public class Reporte_Empleados extends javax.swing.JInternalFrame {

    Logica_Empleado le = new Logica_Empleado();
    ArrayList<String> lista_empleados = new ArrayList<String>();
    Empleado e = new Empleado();
    ImageIcon icon = new ImageIcon("src/multimedia/icono_salir.png");

    public Reporte_Empleados() {
        initComponents();
        setTitle("Reporte de Empleados");
        Mostrar_Empleados();
    }

    public void Mostrar_Empleados() {

        lista_empleados = le.Obtener_Empleado(e);
        int t = lista_empleados.size();

        if (lista_empleados.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Error, No hay Datos Disponibles en el Sistema", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            String nombreC[] = {"Cédula", "Nombre", "Apellidos", "Fecha Nacimiento",
                "Télefono", "Departamento"};
            String datos[][] = new String[t][6];
            String[] separar = new String[6];
            for (int i = 0; i < lista_empleados.size(); i++) {
                separar = lista_empleados.get(i).split("#");
                datos[i][0] = separar[0];
                datos[i][1] = separar[1];
                datos[i][2] = separar[2];
                datos[i][3] = separar[3];
                datos[i][4] = separar[4];
                datos[i][5] = separar[5];
            }
            this.empleados.setModel(new DefaultTableModel(datos, nombreC));
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        empleados = new javax.swing.JTable();
        boton_salir = new javax.swing.JButton();
        boton_actualizar = new javax.swing.JButton();

        setBackground(new java.awt.Color(78, 78, 78));
        setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        empleados.setBackground(new java.awt.Color(144, 144, 144));
        empleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        empleados.setEnabled(false);
        empleados.setRowHeight(20);
        empleados.setRowMargin(5);
        jScrollPane1.setViewportView(empleados);

        boton_salir.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_salir.setForeground(new java.awt.Color(254, 254, 254));
        boton_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_salir_ventana.png"))); // NOI18N
        boton_salir.setText("Salir");
        boton_salir.setBorderPainted(false);
        boton_salir.setContentAreaFilled(false);
        boton_salir.setDefaultCapable(false);
        boton_salir.setFocusPainted(false);
        boton_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_salirActionPerformed(evt);
            }
        });

        boton_actualizar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_actualizar.setForeground(new java.awt.Color(254, 254, 254));
        boton_actualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/boton_actualizar.png"))); // NOI18N
        boton_actualizar.setText("Actualizar");
        boton_actualizar.setBorderPainted(false);
        boton_actualizar.setContentAreaFilled(false);
        boton_actualizar.setDefaultCapable(false);
        boton_actualizar.setFocusPainted(false);
        boton_actualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_actualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 815, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(boton_actualizar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(boton_salir)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(boton_salir)
                    .addComponent(boton_actualizar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void boton_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_salirActionPerformed

        int opcion = JOptionPane.showConfirmDialog(this, "¿Deseas cerrar el programa?", "Pregunta",
            JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, icon);
        if (opcion == JOptionPane.OK_OPTION) {
            this.dispose();
        }
    }//GEN-LAST:event_boton_salirActionPerformed

    private void boton_actualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_actualizarActionPerformed
        Mostrar_Empleados();
    }//GEN-LAST:event_boton_actualizarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boton_actualizar;
    private javax.swing.JButton boton_salir;
    private javax.swing.JTable empleados;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
