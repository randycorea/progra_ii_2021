package vista;

import entidades.Usuario;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import logica.Logica_Empleado;
import logica.Logica_Usuario;

public class Usuarios extends javax.swing.JInternalFrame {

    Usuario u = new Usuario();
    Logica_Usuario lu = new Logica_Usuario();
    Logica_Empleado le = new Logica_Empleado();
    ImageIcon icon = new ImageIcon("src/multimedia/icono_salir.png");
    ArrayList<String> lista_usuarios = new ArrayList<String>();

    public Usuarios() {
        initComponents();
        setTitle("Usuarios");
        Llenar_Combo_Nombres();
        Mostrar_Usuarios();
    }

    public void Llenar_Combo_Nombres() {

        ArrayList<String> lista_nombres = le.Obtener_NombreCompleto_Empleado();

        for (int i = 0; i < lista_nombres.size(); i++) {
            this.combo_nombre_empleados.addItem(lista_nombres.get(i));
        }

    }

    public void seleccionar_datos() {

        int c = this.usuarios.getSelectedRow();

        this.txt_usuario.setText(this.usuarios.getValueAt(c, 0).toString());
        this.txt_contrasenna.setText(this.usuarios.getValueAt(c, 1).toString());

    }

    public void Mostrar_Usuarios() {

        lista_usuarios = lu.Obtener_Usuario(u);
        int t = lista_usuarios.size();

        if (lista_usuarios.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Error, No hay Datos Disponibles en el Sistema", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            String nombreC[] = {"Usuario", "Contraseña", "Tipo", "Nombre del Empleado"};
            String datos[][] = new String[t][4];
            String[] separar = new String[4];
            for (int i = 0; i < lista_usuarios.size(); i++) {
                separar = lista_usuarios.get(i).split("#");
                datos[i][0] = separar[0];
                datos[i][1] = separar[1];
                datos[i][2] = separar[2];
                datos[i][3] = separar[3];
            }
            this.usuarios.setModel(new DefaultTableModel(datos, nombreC));
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txt_usuario = new javax.swing.JTextField();
        txt_contrasenna = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        combo_nombre_empleados = new javax.swing.JComboBox<>();
        combo_tipo = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        usuarios = new javax.swing.JTable();
        boton_agregar = new javax.swing.JButton();
        boton_modificar = new javax.swing.JButton();
        boton_eliminar = new javax.swing.JButton();
        boton_salir = new javax.swing.JButton();

        setBackground(new java.awt.Color(78, 78, 78));
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(254, 254, 254));
        jLabel1.setText("Empleados");

        jLabel3.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(254, 254, 254));
        jLabel3.setText("Registrode Empleados");

        jPanel1.setBackground(new java.awt.Color(144, 144, 144));

        txt_usuario.setBackground(new java.awt.Color(60, 60, 60));
        txt_usuario.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        txt_usuario.setForeground(new java.awt.Color(254, 254, 254));
        txt_usuario.setMinimumSize(new java.awt.Dimension(17, 35));

        txt_contrasenna.setBackground(new java.awt.Color(60, 60, 60));
        txt_contrasenna.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        txt_contrasenna.setForeground(new java.awt.Color(254, 254, 254));
        txt_contrasenna.setMinimumSize(new java.awt.Dimension(17, 35));

        jLabel2.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(254, 254, 254));
        jLabel2.setText("Usuario:");

        jLabel4.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(254, 254, 254));
        jLabel4.setText("Contraseña:");

        jLabel5.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(254, 254, 254));
        jLabel5.setText("Nombre de Empleado:");

        jLabel8.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(254, 254, 254));
        jLabel8.setText("Tipo:");

        combo_nombre_empleados.setBackground(new java.awt.Color(60, 60, 60));
        combo_nombre_empleados.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        combo_nombre_empleados.setForeground(new java.awt.Color(254, 254, 254));

        combo_tipo.setBackground(new java.awt.Color(60, 60, 60));
        combo_tipo.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        combo_tipo.setForeground(new java.awt.Color(254, 254, 254));
        combo_tipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Soporte", "Ventas", "Inventario" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel8))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_contrasenna, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                    .addComponent(txt_usuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(combo_nombre_empleados, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(combo_tipo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(txt_usuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_contrasenna, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(30, 30, 30)
                        .addComponent(jLabel5))
                    .addComponent(combo_nombre_empleados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(combo_tipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        usuarios.setBackground(new java.awt.Color(144, 144, 144));
        usuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        usuarios.setRowHeight(20);
        usuarios.setRowMargin(5);
        usuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                usuariosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(usuarios);

        boton_agregar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_agregar.setForeground(new java.awt.Color(254, 254, 254));
        boton_agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_agregar.png"))); // NOI18N
        boton_agregar.setText("Agregar");
        boton_agregar.setBorderPainted(false);
        boton_agregar.setContentAreaFilled(false);
        boton_agregar.setDefaultCapable(false);
        boton_agregar.setFocusPainted(false);
        boton_agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_agregarActionPerformed(evt);
            }
        });

        boton_modificar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_modificar.setForeground(new java.awt.Color(254, 254, 254));
        boton_modificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_modificar_boton.png"))); // NOI18N
        boton_modificar.setText("Modificar");
        boton_modificar.setBorderPainted(false);
        boton_modificar.setContentAreaFilled(false);
        boton_modificar.setDefaultCapable(false);
        boton_modificar.setFocusPainted(false);
        boton_modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_modificarActionPerformed(evt);
            }
        });

        boton_eliminar.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_eliminar.setForeground(new java.awt.Color(254, 254, 254));
        boton_eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/sacar_producto.png"))); // NOI18N
        boton_eliminar.setText("Eliminar");
        boton_eliminar.setBorderPainted(false);
        boton_eliminar.setContentAreaFilled(false);
        boton_eliminar.setDefaultCapable(false);
        boton_eliminar.setFocusPainted(false);
        boton_eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_eliminarActionPerformed(evt);
            }
        });

        boton_salir.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        boton_salir.setForeground(new java.awt.Color(254, 254, 254));
        boton_salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multimedia/icono_salir_ventana.png"))); // NOI18N
        boton_salir.setText("Salir");
        boton_salir.setBorderPainted(false);
        boton_salir.setContentAreaFilled(false);
        boton_salir.setDefaultCapable(false);
        boton_salir.setFocusPainted(false);
        boton_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton_salirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addGap(92, 92, 92)
                        .addComponent(boton_agregar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 277, Short.MAX_VALUE)
                        .addComponent(boton_modificar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(boton_eliminar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(boton_salir))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel1)
                        .addGap(6, 6, 6)
                        .addComponent(jLabel3))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(boton_eliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(boton_salir)
                                .addComponent(boton_modificar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(boton_agregar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void boton_agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_agregarActionPerformed
        if (this.txt_usuario.getText().equals("") || this.txt_contrasenna.getText().equals("")
                || this.combo_tipo.getSelectedItem() == null || this.combo_nombre_empleados.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(this, "No puede dejar ningun campo vacío", "Error",
                    JOptionPane.ERROR_MESSAGE, icon);
        } else {
            u.setUsuario(this.txt_usuario.getText());
            u.setContrasenna(this.txt_contrasenna.getText());
            u.setTipo((String) this.combo_tipo.getSelectedItem());
            u.setNombre_empleado((String) this.combo_nombre_empleados.getSelectedItem());

            boolean validacion = lu.Agregar_Usuario(u);
            if (validacion == true) {
                JOptionPane.showMessageDialog(this, "Se guardarón correctamente los datos", "Exitoso",
                        JOptionPane.INFORMATION_MESSAGE);
                this.txt_usuario.setText("");
                this.txt_contrasenna.setText("");
                this.txt_usuario.requestFocus();
                Mostrar_Usuarios();
            } else {
                JOptionPane.showMessageDialog(this, "No se pudo guardar los datos en la Base de Datos", "Error",
                        JOptionPane.ERROR_MESSAGE, icon);
            }
        }
    }//GEN-LAST:event_boton_agregarActionPerformed

    private void boton_modificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_modificarActionPerformed

        if (this.txt_usuario.getText().equals("") || this.txt_contrasenna.getText().equals("")
                || this.combo_tipo.getSelectedItem() == null || this.combo_nombre_empleados.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(this, "No puede dejar ningun campo vacío", "Error",
                    JOptionPane.ERROR_MESSAGE, icon);
        } else {
            u.setUsuario(this.txt_usuario.getText());
            u.setContrasenna(this.txt_contrasenna.getText());
            u.setTipo((String) this.combo_tipo.getSelectedItem());
            u.setNombre_empleado((String) this.combo_nombre_empleados.getSelectedItem());

            boolean validacion = lu.Modificar_Usuario(u);
            if (validacion == true) {
                JOptionPane.showMessageDialog(this, "Se guardarón correctamente los datos", "Exitoso",
                        JOptionPane.INFORMATION_MESSAGE);
                this.txt_usuario.setText("");
                this.txt_contrasenna.setText("");
                this.txt_usuario.requestFocus();
                Mostrar_Usuarios();
            } else {
                JOptionPane.showMessageDialog(this, "No se pudo guardar los datos en la Base de Datos", "Error",
                        JOptionPane.ERROR_MESSAGE, icon);
            }
        }

        Mostrar_Usuarios();
    }//GEN-LAST:event_boton_modificarActionPerformed

    private void boton_eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_eliminarActionPerformed
        int c;
        try {
            c = this.usuarios.getSelectedRow();
            u.setUsuario(this.usuarios.getValueAt(c, 0).toString());
            u.setContrasenna(this.usuarios.getValueAt(c, 1).toString());
            u.setTipo(this.usuarios.getValueAt(c, 2).toString());
            u.setNombre_empleado(this.usuarios.getValueAt(c, 3).toString());
            boolean validacion = lu.Eliminar_Usuario(u);
            if (validacion == true) {
                JOptionPane.showMessageDialog(this, "Datos Eliminados Correctamente", "Exitoso", JOptionPane.INFORMATION_MESSAGE);
                Mostrar_Usuarios();
            } else {
                JOptionPane.showMessageDialog(this, "Error no se pudo eliminar el registro",
                        "Error", JOptionPane.ERROR_MESSAGE, icon);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Para eliminar el registro debes primero seleccionar una fila de la tabla",
                    "Error", JOptionPane.ERROR_MESSAGE, icon);
        }
    }//GEN-LAST:event_boton_eliminarActionPerformed

    private void boton_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton_salirActionPerformed

        int opcion = JOptionPane.showConfirmDialog(this, "¿Deseas cerrar el programa?", "Pregunta",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, icon);
        if (opcion == JOptionPane.OK_OPTION) {
            this.dispose();
        }
    }//GEN-LAST:event_boton_salirActionPerformed

    private void usuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_usuariosMouseClicked
        seleccionar_datos();
    }//GEN-LAST:event_usuariosMouseClicked

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Usuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Usuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Usuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Usuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Usuarios().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boton_agregar;
    private javax.swing.JButton boton_eliminar;
    private javax.swing.JButton boton_modificar;
    private javax.swing.JButton boton_salir;
    private javax.swing.JComboBox<String> combo_nombre_empleados;
    private javax.swing.JComboBox<String> combo_tipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txt_contrasenna;
    private javax.swing.JTextField txt_usuario;
    private javax.swing.JTable usuarios;
    // End of variables declaration//GEN-END:variables
}
