package entidades;

public class Producto {
    
    private int id;
    private String codigo;
    private String nombre;
    private int cantidad;
    private float precio_unitario;
    private float precio_total_productos;
    
    public Producto(){
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(float precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public float getPrecio_total_productos() {
        return precio_total_productos;
    }

    public void setPrecio_total_productos(float precio_total_productos) {
        this.precio_total_productos = precio_total_productos;
    }
    
}
